# My Carshop

El módulo ofrece un sistema de control de préstamos de vehículos. Como coches de substitución por parte de una empresa o una persona muy generosa.

Podemos introducir nuevas marcas y categorías de vehículo. Al cargar el módulo ya dispondremos de una serie de elementos.

En la vista de un vehículo, si vamos a crear un nuevo préstamo, el coche se cargará automaticaménte en su campo. Y si añadimos un préstamo a un coche disponible, este pasará a prestado. Si se intenta prestar un coche que aun está prestado, nos lo impedirá.

La vista vehículo tiene su vista kanban, agrupada por las categorías de coches (SEDAN, COUPE...). La kanban card de cada vehículo tiene una banda de color a la izquierda que indica su estado. Y cada columna tiene arriba una barra de progreso para ver un total de los coches, divididos por su estado.

Dentro de cada préstamo podremos indicar, o no, si la actividad de uso del coche será ilegal, legal o ambas. Muy importante para conocer a nuestros clientes y/o conocidos.

La gestión de clientes está relacionada con en el módelo res.partner, para mayor interelación con el resto de funciones de odoo.

La vista de préstamos tiene una vista calendario, para ver de una forma más dinámica el histórico de préstamos. 

# Futuras actualizaciones

- Mejorar el control de estados en los vehículos.
