# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)

class CarshopCar(models.Model):
    _name = 'carshop.car'
    _description = 'Carshop Car'
    name = fields.Char('Name', required=True)
    date_adquire = fields.Date('Adquisition Date')
    brand_ids = fields.Many2one('carshop.car.brand', string='Brands')
    category_id = fields.Many2one('carshop.car.category', string='Category')
    state = fields.Selection([
        ('draft', 'Unavailable'),
        ('available', 'Available'),
        ('onMaintenance', 'On Maintenance'),
        ('borrowed', 'Borrowed')],
        'State', default="draft")

    is_lent = fields.Boolean('Lent', compute='check_lent', default=False)

    loan_ids = fields.One2many('carshop.loan', inverse_name='car_id')
    
    car_image = fields.Binary('Picture')

    @api.onchange('name')     
    def get_func(self):
        if self.name != False:
            if self.env['carshop.car'].search([('name', "=", self.name)]).name == False:
                self.env['ir.config_parameter'].set_param('nombre', self.name)
            else:
                message = _('Car name %s is already registered') % (self.name)
                self.name = ''
                raise UserError(message)
        else:
            self.env['ir.config_parameter'].set_param('nombre', '')
        return

    @api.multi
    def check_lent(self):
        for car in self:
            domain = ['&',('car_id.id', '=', car.id), ('date_end', '>=', datetime.now())]
            car.is_lent = self.env['carshop.loan'].search(domain, count=True) > 0
            self.env['ir.config_parameter'].set_param('nombre', car.name)
            if car.is_lent and car.state != 'borrowed':
                car.is_lent = False

    @api.model
    def is_allowed_transition(self, old_state, new_state):
        allowed = [('draft', 'available'),
                   ('available', 'draft'),
                   ('available', 'borrowed'),
                   ('onMaintenance', 'available'),
                   ('onMaintenance', 'draft'),
                   ('borrowed', 'borrowed'),
                   ('available', 'onMaintenance'),
                   ('borrowed', 'available')]
        return (old_state, new_state) in allowed

    @api.multi
    def change_state(self, new_state):
        for car in self:
            if car.is_allowed_transition(car.state, new_state):
                car.state = new_state
            else:
                message = _('Moving from %s to %s is not allowed') % (car.state, new_state)
                raise UserError(message)

    def make_available(self):
        self.change_state('available')

    def make_unavailable(self):
        self.change_state('draft')
        
    def make_on_maintenance(self):
        self.change_state('onMaintenance')

    def make_borrowed(self):
        self.change_state('borrowed')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(CarshopCar, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        self.create_categories()
        self.create_brands()
        return res

    def create_brands(self):
        if self.env['carshop.car.brand'].search([('name', '=' ,'MERCEDES')]).name == False:
            mercedes = {
                'name': 'MERCEDES',
                'phone': '678345724'
            }
            audi = {
                'name': 'AUDI',
                'phone': '612378291'
            }
            ford = {
                'name': 'FORD',
                'phone': '689090212'
            }
            record = self.env['carshop.car.brand'].create(mercedes)
            record = self.env['carshop.car.brand'].create(audi)
            record = self.env['carshop.car.brand'].create(ford)
        return True

    def create_categories(self):
        if self.env['carshop.car.category'].search([('name', '=' ,'SEDAN')]).name == False:
            sedan = {
                'name': 'SEDAN',
                'description': 'A sedan has four doors and a traditional trunk. Like vehicles in many categories, they\'re available in a range of sizes from small, compacts, mid-size...'
            }
            muscle_car = {
                'name': 'MUSCLE CAR',
                'description': 'high-performance American coupes, usually but not limited to rear-wheel drive and fitted with a large displacement V8 engine.'
            }
            business = {
                'name': 'BUSINESS',
            'description': 'A two-door car with no rear seat or with a removable rear seat intended for traveling salespeople and other vendors carrying their wares with them.'
            }
            coupe = {
                'name': 'COUPE',
                'description': 'A coupe has historically been considered a two-door car with a trunk and a solid roof.',
                'child_ids': [
                    (0, 0, muscle_car),
                    (0, 0, business),
                ]
            }
            convertible = {
                'name': 'CONVERTIBLE',
                'description': 'Does the roof retract into the body leaving the passenger cabin open to the elements? If so, it\'s a convertible. Most convertibles have a fully powered fabric roof that folds down, but a few have to be lowered by hand.'
            }
            sports_car = {
                'name': 'SPORTS CAR',
                'description': 'These are the sportiest, hottest, coolest-looking coupes and convertibles—low to the ground, sleek, and often expensive. They generally are two-seaters, but sometimes have small rear seats as well.',
                'child_ids': [
                    (0, 0, convertible)
                ]
            }
            record = self.env['carshop.car.category'].create(sedan)
            record = self.env['carshop.car.category'].create(coupe)
            record = self.env['carshop.car.category'].create(sports_car)
        return True

    @api.multi
    def find_car(self):
        domain = [
            '|',
                '&', ('name', 'ilike', 'Car Name'),
                     ('category_id.name', '=', 'Category Name'),
                '&', ('name', 'ilike', 'Car Name 2'),
                     ('category_id.name', '=', 'Category Name 2')
        ]
        cars = self.search(domain)
        logger.info('Cars found: %s', cars)
        return True

    @api.model
    def get_all_carshop_customers(self):
        carshop_customer_model = self.env['carshop.customer']
        return carshop_customer_model.search([])



class CarshopCustomer(models.Model):
    _name = 'carshop.customer'
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one('res.partner', ondelete='cascade')
    date_start = fields.Date('Customer Since')
    date_end = fields.Date('Termination Date')
    customer_number = fields.Char()
    date_of_birth = fields.Date('Date of birth')

class CarshopLoan(models.Model):
    _name = 'carshop.loan'
    _description = 'Carshop Loan'
    _rec_name = 'car_id'
    _order = 'date_end desc'
    car_id = fields.Many2one('carshop.car', required=True, default=lambda self: self.env['carshop.car'].search([('name', '=', self.env['ir.config_parameter'].get_param('nombre'))]))
    customer_id = fields.Many2one('carshop.customer', required=True)
    date_start = fields.Date('Loan Start', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    date_end = fields.Date('Loan End', default=lambda *a:(datetime.now() + timedelta(days=(2))).strftime('%Y-%m-%d'))
    actividad_id = fields.Selection((('i','Illegal'), ('l','Legal'), ('b', 'Both')), 'Type of Activity')

    customer_image = fields.Binary('Customer Image', related='customer_id.partner_id.image')

    @api.constrains('car_id')
    def _check_car_id(self):
        for loan in self:
            car = loan.car_id
            domain = ['&',('car_id.id', '=', car.id), ('date_end', '>=', datetime.now())]
            car.is_lent = self.search(domain, count=True) > 1 
            if car.is_lent:
                raise models.ValidationError('Car is Lent!')
            else:
                car.change_state('borrowed')
    
    @api.constrains('date_end', 'date_start')
    def _check_dates(self):
        for loan in self:
            if loan.date_start > loan.date_end:
                raise models.ValidationError('Start date After end date!')