# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class CarBrand(models.Model):
    _name = 'carshop.car.brand'

    name = fields.Char('Brand')
    phone = fields.Char('Contact phone')
