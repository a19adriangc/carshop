# -*- coding: utf-8 -*-
{
    'name': "My Carshop",  # Module title
    'summary': "Manage Cars Easily",  # Module subtitle phrase
    'description': """Long description""",  # You can also rst format
    'author': "Adrián García Castro",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/carshop_car.xml',
        'views/carshop_car_categ.xml',
        'views/carshop_loan.xml',
        'views/carshop_car_brand.xml',
        'views/carshop_customer.xml'
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}
